# -*- coding: utf-8 -*-
"""Console script for zeabox."""
import logging
import os
import sys
from shutil import copyfile
import yaml
import click


from zeabox import io, ui, zeabox
from zeabox.strings import resolve_output_path
from zeabox.logging import setup_logging

DEFAULT_CONFIG = os.path.join(os.path.dirname(os.path.realpath(__file__)), "config.yaml")

logger = logging.getLogger(__name__)


class Context(object):
    def __init__(self):
        self.cfg = {}

pass_context = click.make_pass_decorator(Context, ensure=True)


@click.group()
@click.option("-c", "--config", type=click.Path())
@pass_context
def main(ctx, config):
    with open(DEFAULT_CONFIG) as f:
        ctx.cfg = yaml.load(f)
    if config:
        with open(config) as f:
            print(config)
            ctx.cfg.update(yaml.load(f))

    setup_logging()


@main.command()
@click.option("--file", type=click.Path(), default="config.yaml", help="configuration file name")
@click.option('--override/--no-override', default=False, help="force overriding the existing configuration file with default value")
def configure(file, override):
    """Provide the default configuration file for user customization"""
    if not os.path.isfile(file) or override:
        copyfile(DEFAULT_CONFIG, "config.yaml")
        ui.info("Default configuration file copied to {0}".format(file))
    else:
        ui.warning("Configuration file already exist. If you want to override it use the '--override' flag")


@main.command()
@pass_context
def live(ctx):
    """Live data acquisition and processing from devices.
    """
    ui.start_zeamage()
    ui.info(ui.LIVE_MODE)
    zeabox.live_mode(ctx.cfg)
    ui.stop_zeamage()


@main.command()
@click.option("-o", "--output", type=click.Path(), default="./outputs",
    show_default=True,
    help='''output directory''')
@click.option('-O','--override', is_flag=True, help='''override output directory''')
@click.option('-p','--process', is_flag=True, help='''Activate/Diseable the processing of device outputs. Usefull to capture image only when disable''')
@click.option('-s','--summarize', is_flag=True, help='''Activate/Diseable the processing and summarizing of device outputs.''', default=False)
@click.option(
    '-i', '--item',
    type=click.Choice(["ear", "grain"]),
    default="ear",
    help='''item type to be found and measured on images''')
@pass_context
def snapshot(ctx,  output, override, item, process, summarize):
    """Data acquisition and processing from connected devices.
    """
    ui.start_zeamage()
    ui.info(ui.SNAPSHOT_MODE)
    ui.info("Measuring {}".format(item))
    output_path = resolve_output_path(output, override)
    zeabox.snapshot_mode(ctx.cfg, item, output_path, process, summarize)
    ui.stop_zeamage()

@main.command()
@click.option("-s", "--src", type=click.Path(),
    default = "",
    help='''source file or directory''')
@click.option("-o", "--output", type=click.Path(), default="./outputs",
    show_default=True,
    help='''output directory''')
@click.option('-O','--override', is_flag=True, help='''override output directory''')
@click.option(
    '-i', '--item',
    type=click.Choice(["ear", "grain"]),
    default="ear",
    help='''item type to be found and measured on images''')
@pass_context
def process(ctx, src, output, override, item):
    """Process image files.

    If path is none, use current cwd()

    """

    ui.start_zeamage()
    ui.message(ui.BATCH_MODE )
    output_path = resolve_output_path(output, override)
    zeabox.batch_mode(ctx.cfg, item, src, output_path)
    ui.stop_zeamage()



if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
