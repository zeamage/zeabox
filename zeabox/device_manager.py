from zeabox import ui
from zeabox.error import NoDeviceError
from zeabox import device as zea_device


class DeviceManager:
    """ Define interface for device object connected through USB"""

    video_lst = ["webcam", "video"]
    scale_lst = ["serialscale"]

    def __init__(self, csv_file_path=None, lookup=None, usb_lib=None, cfg=None):
        # self._find_devices()

        if not usb_lib:
            import usb.core
            usb_lib = usb.core

        if not lookup:
            from zeabox import usb_ids
            usb_device = usb_ids.USB_IDS
            know_device = usb_ids.KNOWN_DEVICE

        self.cfg = cfg
        self._usb_device = usb_device
        self._known_device = know_device
        self._usb = usb_lib

        # initialize device
        self.devices = []
        self.find()
        self.connect()

    def find(self):
        """
        Finds the devices that could be used to measure things
        Raise an error if none are found if none are found.
        """
        available_devices = self.find_available()

        for device in available_devices:
            # Read configuration file: we force device use from configuration
            # TODO
            # check if device is in KNOWN_DEVICE:
            if self.is_known_device(device):
                info = self.to_info(device, self._known_device)
                self.add_device(device, info)
                continue

            # Try to guess other device
            # TODO

        if not self.devices:
            raise NoDeviceError("No usable device has been detected!")

    def to_info(self, device, lookup):
        info = {
            "vendor": lookup[device.idVendor]["name"],
            "product": lookup[device.idVendor][device.idProduct]
        }
        info["type"] = self.device_type(info)
        return info

    def device_type(self, device_info):
        """Define device type base on product description."""
        product = device_info["product"].lower()
        if any(word in product for word in DeviceManager.scale_lst):
            return "serialscale"
        if any(word in product for word in DeviceManager.video_lst):
            return "video"

    def is_known_device(self, device):
        return device.idVendor in self._known_device \
            and device.idProduct in self._known_device[device.idVendor]

    def find_available(self):
        """
        Finds all available plugged USB devices.
        Returns None if no devices are available.
        """
        devices = self._usb.find(find_all=True)

        # Returns none if no devices are available.
        if not devices:
            raise NoDeviceError()

        return devices

    def add_device(self, device, info):
        if info['type'] == "video":
            self.devices.append(zea_device.Camera(device, info, self.cfg))
            ui.ok("Camera added: '{0}'".format(info["product"]))
        if info['type'] == "serialscale":
            self.devices.append(zea_device.SerialScale(device, info, self.cfg))
            ui.ok("Scale added: '{0}'".format(info["product"]))

    def connect(self):
        for d in self.devices:
            d.connect()

    def disconnect(self):
        for d in self.devices:
            d.disconnect()

    def info(self):
        for d in self.devices:
            print(d.info)

    def read(self):
        data = {}
        for d in self.devices:
            data_device = d.read()
            # if isinstance(data_device, list): # come from zeamage
            #     zea_data = data_device
            # else:
            data.update(data_device)

        # data_devices = []
        # if zea_data:
        #     for d in zea_data:
        #         d.update(data)
        #         data_devices.append(d)
        #     return data_devices
        return data


    def watch(self):
        for d in self.devices:
            d.watch()
