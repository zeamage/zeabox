import logging
import os
import click
import cv2
from zeabox import ui
from zeabox.strings import reverse_format


logger = logging.getLogger(__name__)

EXTENSION = (".png", ".jpg", ".JPG")


class ImageManager:
    """ImageManager deal with input path image or directory.
        It normalizes images (format, size, orientation) before they are processed by zeamge lib
        and return outputs
    """
    def __init__(self, cfg):
        self.filename_fmt = cfg["filename"]
        self.img_width = cfg["width"]
        self.images = []


    def read(self, path):
        """Read path"""
        if os.path.isdir(path):
            filenames = [f for f in os.listdir(path) if f.endswith(EXTENSION)]
            nb_files = len(filenames)
            with ui.progress_bar(filenames, nb_files, "(1) Loading {} files\t".format(nb_files)) as files:
                for filename in files:
                    self.read_file(
                        os.path.join(path, filename))
        elif os.path.isfile(path):
            ui.message("\t\t(1) Reading file")
            self.read_file(path)
        else:
            ui.error("File not found")


    def read_file(self, path):
        """Read image file and resize it"""
        # Id based on file name
        logger.info('Reading image: {}'.format(path))

        file_w_ext = os.path.basename(path)
        file_name, file_ext = os.path.splitext(file_w_ext)

        # Store information from file name
        data = {"filename": file_name}
        if self.filename_fmt:
            data.update(reverse_format(self.filename_fmt, file_name))

        #Check file validity: extension, size, orientation
        # read file
        try:
            img = cv2.imread(path)
            # Image validity
            if img is None:
                raise Exception('The Image file is not ok')
            img = self.normalize_img(img)
            data["img"] = img
        except Exception as e:
            logger.error('Failed to open file', exc_info=True)

        self.images.append(data)


    def view(self):
        for i in self.images:
            cv2.imshow(i["name"], i["img"])
            cv2.waitKey(0)
            cv2.destroyAllWindows()


    def normalize_img(self, img=None):
        """Normalize imgage: size, orientation"""
        # Orientation
        img = self.to_landscape(img)
        # resizing the image
        img = self.resize(img, width=self.img_width)
        return img


    def to_landscape(self, img):
        (h, w) = img.shape[:2]
        if h > w:
            img = cv2.transpose(img)
            img = cv2.flip(img, 1)
        return img

    def resize(self, img, width=None, height=None, inter=cv2.INTER_AREA):
        if width is None and height is None:
            return
        # initialize the dimensions of the image to be resized and
        # grab the image size
        dim = None
        (h, w) = img.shape[:2]

        # if both the width and height are None, then return the
        # original image
        if width is None and height is None:
            return img

        # check to see if the width is None
        if width is None:
            # calculate the ratio of the height and construct the
            # dimensions
            r = height / float(h)
            dim = (int(w * r), height)

        # otherwise, the height is None
        else:
            # calculate the ratio of the width and construct the
            # dimensions
            r = width / float(w)
            dim = (width, int(h * r))

        # resize the image
        return cv2.resize(img, dim, interpolation=inter)
