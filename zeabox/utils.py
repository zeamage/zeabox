import collections


def flatten(self, d, parent_key='', sep='_'):
    """Flatten nested dictionnary."""
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + str(k) if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(self.flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)
