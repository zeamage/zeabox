﻿import sys
import re
import time
import serial
from abc import ABC, abstractmethod
import logging

import cv2

from zeabox import ui
from zeabox.error import NoDeviceError

logger = logging.getLogger(__name__)

# CONFIG = {
#     'CSV_DELIMITER':','
# }
CAMERA_NOT_FOUND = "camera/webcam not found"
CAMERA_NOT_CONNECTED = "camera/webcam not connect"


class Device(ABC):
    """ Absctract class for any acquisition device """

    def __init__(self, device, info, cfg=None):
        self.cfg = cfg
        self.device = device
        self.info = info
        self.values = [
        ]  # list of measures, usefull in continuous mode measure for summary
        self.value = None  # last measured
        super(Device, self).__init__()

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def disconnect(self):
        pass

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def watch(self):
        pass

    @abstractmethod
    def write(self):
        """Return the last measure, i.e. value"""
        pass


class Camera(Device):
    def connect(self):
        self.crop_height = 0
        if "image" in self.cfg.keys():
            if "crop_height" in  self.cfg["image"].keys():
                self.crop_height =  self.cfg["image"]["crop_height"]

        webcam_number = 0
        if "device" in self.cfg.keys():
            if "webcam_number" in self.cfg["device"].keys():
                webcam_number = self.cfg["device"]["webcam_number"]
        # FIXME make zooming ok
        self.camera = cv2.VideoCapture(webcam_number)
        if self.camera is None:
            ui.error(CAMERA_NOT_FOUND)
            sys.exit(0)
        else:
            self.camera.set(3, 1280)
            self.camera.set(4, 720)
            self.autofocus()

    def crop_frame(self, frame):
        ch = self.crop_height
        h, w = frame.shape[:2]
        return frame[ch:h-ch, 0:w]

    def disconnect(self):
        if self.camera:
            self.camera.release()
            cv2.destroyAllWindows()
        else:
            ui.error(CAMERA_NOT_CONNECTED)

    def read(self):
        if self.camera:
            for _ in range(5):  # hack to empty camera image buffering
                ret, frame = self.camera.read()
            if ret is True:
                self.value = self.crop_frame(frame)
                return {"img_input":self.value}
        else:
            ui.error(CAMERA_NOT_CONNECTED)
            sys.exit(0)

    def watch(self):
        key = cv2.waitKey(1) & 0xFF
        if self.camera:
            # for _ in range(5):  # hack to empty camera image buffering
            ret, frame = self.camera.read()
            if ret is True:
                frame = self.crop_frame(frame)
                # TODO do live processing HERE and display resulting image/frame
                cv2.imshow("Zeabox: live mode", frame)
        else:
            ui.error(CAMERA_NOT_CONNECTED)
            sys.exit(0)

    def write(self):
        return self.value

    def info(self):
        print("camera")

    def autofocus(self):
        """ At device connection, autofocus is set using a timer"""
        timeout = 8  # second
        timeout_start = time.time()
        while time.time() < timeout_start + timeout:
            ret, frame = self.camera.read()
            frame = self.crop_frame(frame)
            if ret is True:
                fm = round(self.variance_of_laplacian(frame), 0)
                text = round(timeout - (time.time() - timeout_start), 0)
                cv2.putText(frame, "Set autofocus: {}".format(text), (10, 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
                cv2.putText(frame, "Blur: {:.2f}".format(fm), (10, 60),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
                cv2.imshow("Autofocus...", frame)
            else:
                ui.error(CAMERA_NOT_CONNECTED)
                sys.exit(0)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cv2.destroyAllWindows()
        ui.ok("Autofocus done")

    def variance_of_laplacian(self, image):
        """Compute the variance of the Laplacian of the image."""
        return cv2.Laplacian(image, cv2.CV_64F).var()


class SerialScale(Device):
    def connect(self, baudrate=9600):
        if self.device.is_kernel_driver_active(0):
            try:
                device.detach_kernel_driver(0)
                logger.info("Serial scale device kernel driver detached")
            except usb.core.USBError as e:
                sys.exit("Could not detach  serial scale device kernel driver")
        else:
            logger.info("No serial scale logger.infoievNces kernel driver attached")

        self.device.set_configuration()


    def disconnect(self):
        pass

    def read(self):
        endpoint = device[0][(0,0)][0]
        raw_values = []

        while True:
            try:
                data = device.read(endpoint.bEndpointAddress,
                                   endpoint.wMaxPacketSize)
                new_RxData = ''.join([chr(x) for x in data[2:]])
                if new_RxData:
                    RxData += new_RxData
                    p = re.findall("[- ]*?[0-9]*\.?[0-9]+", RxData)
                else:
                    break
            except usb.core.USBError as e:
                data = None
                if e.args == ('Operation timed out',):
                    continue

        if len(p)>0:
            self.value = float(p[-1].replace(" ", ""))
        return {"weight":self.value}


    def watch(self):
        pass
        # if (self.ser.inWaiting()>0):
        #     self.value = self.ser.readline().decode()
        #     # FIXME detect ZERO value before printing
        #     self.print()

    def write(self):
        """Return the last measure, i.e. value"""
        pass

    def print(self):
        ui.measure(self.value)

    def format(self, raw_value):
        xraw_value = ''.join([chr(x) for x in raw_value[2:]])
        p = re.findall("[- ]*?[0-9]*\.?[0-9]+", xraw_value)
        return float(p[-1].replace(" ", ""))
