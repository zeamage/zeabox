
class ConnectionError(Exception):
    pass


class NoDeviceError(IOError):
    pass
