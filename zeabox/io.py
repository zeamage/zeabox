# -*- coding: utf-8 -*-
"""Main module."""
import os
import cv2
import logging

import zeabox

logger = logging.getLogger(__name__)


def view_image(img):
    cv2.imshow("View", img)
    cv2.waitKey(0)


def read_image(image, resize=None):
    """Read image file and resize it"""
    logger.info('Read image')
    try:
        img = cv2.imread(image)
        if resize is not None:
            print("yop")
            return zeabox.img.resize(img, width=resize)
        return img
    except Exception as e:
        logger.error('Failed to open file', exc_info=True)


def write_image(img, path, file_name="output.jpg"):
    """Write image file the output path."""
    return cv2.imwrite(os.path.join(path, file_name), img)
