# -*- coding: utf-8 -*-
"""Main module."""
import os
import logging

from zeabox import ui
from zeabox.device_manager import DeviceManager
from zeabox.data_manager import DataManager
from zeabox.image_manager import ImageManager
from zeamage.Zeamage import Zeamage
from zeamage.processing import display

logger = logging.getLogger(__name__)


def snapshot_mode(cfg, obj_type, output, process, summarize):
    # Device connection
    devices = DeviceManager(cfg=cfg["zeabox"])
    #
    zeabox_data = []  # all collected data
    zeabox_data_summary = []  # all collected data
    existing_names = []
    if "meta_data" in cfg["zeabox"]:
        dft_extra = cfg["zeabox"]["meta_data"]
        value_extra = dft_extra
        existing_extra = {k: [] for k, _ in value_extra.items()}

    while (True):
        if ui.ask_user():
            name, img_name = ui.ask_for_name(existing_names, cfg["zeabox"]["image"]["filename"])
            existing_names.append(name)
            if "meta_data" in cfg["zeabox"]:
                for k in sorted(dft_extra.keys()):
                    v = dft_extra[k]
                    extra = ui.ask_for_extra(k, v, existing_extra[k])
                    existing_extra[k].append(extra)
                    value_extra[k] = extra

            ui.info('Processing: {}'.format(name))
            device_data = devices.read()

            if process is True or summarize is True:
                items_data = []  # unique item processes
                # process image with zeamage
                if "img_input" in device_data.keys():
                    img = device_data["img_input"]
                    zea = Zeamage(img, cfg["zeamage"])
                    zea.detect_items(obj_type=obj_type)
                    if process is True:
                        zea.describe_items(obj_type=obj_type)
                        zea.measure_items(obj_type=obj_type)
                    zea_data = zea.get_data(with_img=True)

                    if summarize is True:
                        zea_data_summary  = zea.get_data_summary(with_img=True)
                        zea_data_summary.update(device_data)
                        zea_data_summary.update(img_name)
                        if "meta_data" in cfg["zeabox"]:
                            zea_data_summary.update(value_extra)

                if ui.is_valid_image():
                    # items data
                    for d in zea_data:
                        d.update(device_data)
                        d.update(img_name)
                        if "meta_data" in cfg["zeabox"]:
                            d.update(value_extra)
                        zeabox_data.append(d)
                    if summarize is True:
                        # Sumary data
                        zeabox_data_summary.append(zea_data_summary)
            else:
                # check images
                if "img_input" in device_data.keys():
                    display.show_img(device_data["img_input"])
                    if ui.is_valid_image():
                        if summarize is True:
                            zeabox_data_summary.append(zea_data_summary)
        else:
            break

    devices.disconnect()

    if len(zeabox_data) > 0 and process is True:
        dm = DataManager(zeabox_data, output_path=output)
        dm.to_csv()
        if process is True:
            dm.to_normalized_img()
            dm.to_summary_img()
            dm.to_patch_img()
            if summarize is False:
                dm.to_input_img()
    else:
        logger.warning("No items processed")

    if len(zeabox_data_summary) > 0 and summarize is True:
        dm = DataManager(zeabox_data_summary, output_path=output, csv_suffix="summary_")
        dm.to_csv()
        dm.to_input_img()
        dm.to_summary_img()
    else:
        logger.warning("No data summarized")


def live_mode(cfg):
    # Device connection
    devices = DeviceManager(cfg=cfg["zeabox"])
    try:
        while (True):
            devices.watch()
    except KeyboardInterrupt:
        devices.disconnect()


def batch_mode(cfg, obj_type, src, output):
    if src is None:
        src = os.getcwd()

    # ImageManager: read and prepare file
    im = ImageManager(cfg['zeabox']['image'])
    im.read(src)

    # Compute image: ZEAMAGE
    zea_data = []
    nb_img = len(im.images)
    with ui.progress_bar(im.images, nb_img,
                         "(2) Processing {} images".format(nb_img)) as images:
        for img in images:
            logger.debug("Processing image: {}".format(img["filename"]))
            zea = Zeamage(img["img"], cfg["zeamage"])
            zea.detect_items()
            zea.describe_items(obj_type=obj_type)
            zea.measure_items(obj_type=obj_type)

            # make a list of dict
            zdata = zea.get_data(with_img=True)
            for item_data in zdata:
                item_data.update(img)
                zea_data.append(item_data)

    ui.message("\t\t(3) Exporting outputs")
    # Export data csv, normalized images (one items per images) and summary image
    # (normalized image with drawing).  Output types should be optionnal
    #  ------[original_image]
    #       |----- images.png
    #       |----- ...
    #  ------[normalized_image]
    #       |----- images.png
    #       |----- ...
    #  ------[summary_image]
    #       |----- images.png
    #       |----- ...
    #  ------ zea_data.csv

    dm = DataManager(zea_data, output_path=output)
    dm.to_csv()
    dm.to_normalized_img()
    dm.to_summary_img()
    dm.to_patch_img()
