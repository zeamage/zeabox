import datetime
import re
import sys

import click

from zeabox import __version__ as version
from zeabox import ascii_app
from zeabox.strings import reverse_format

MSG_OK = "Image has been processed ! "
MSG_NO_IMG = "No image/directory path has been provided!"
UNKNOW_OBJ_TYPE = "Unknown object to be measured"
UNKNOW_MODE = "Unknown mode, choices are user, watch"
NOT_IMPLEMENTED = "Not implemented yet!"
BATCH_MODE = "Running in batch mode"
SNAPSHOT_MODE = "Running in snapshot mode (webcam)"
LIVE_MODE = "Running in a live mode"


def message(msg, fg="magenta", bg=None):
    click.echo(click.style(msg + "\n", fg=fg, bg=bg), nl=False)


def ok(msg):
    msg = "[   ok    ]\t{}".format(msg)
    message(msg, "green")


def warning(msg):
    msg = "[ warning ]\t{}".format(msg)
    message(msg, "yellow")


def error(msg):
    msg = "[  error  ]\t{}".format(msg)
    message(msg, "red")
    sys.exit(0)


def info(msg):
    msg = "[  info   ]\t{}".format(msg)
    message(msg, "magenta")


def action(msg, helper=None, color="cyan"):
    msg = "[ action  ]\t{}".format(msg)
    message(msg, color)
    if helper:
        message(helper, color)


def measure(msg):
    msg = "[ measure ]\t{}".format(msg)
    message(msg, "green")


def ask_user():
    action(
        "Do you want to proceed?\t[Y/n]",
        helper="\t\t(Press either the 'n', 'q', 'Ctrl-C' to quit.)")
    click.echo()
    c = click.getchar()

    if c in {'q', 'n', '\x03'}:
        return False

    if c.lower() == "y" or c == "\x0D":
        return True
    else:
        warning("Invalid answer !")
        ask_user()

    return True


def is_valid_image():
    action(
        "Are image valid?\t[Y/n]",
        helper="\t\t(Press either the 'n', 'q', 'Ctrl-C' to leave measure.)",
        color="red")
    click.echo()
    c = click.getchar()

    if c in {'q', 'n', '\x03'}:
        return False

    if c.lower() == "y" or c == "\x0D":
        return True
    else:
        warning("Invalid answer !")
        ask_user()

    return True


def ask_for_extra(key, default, existing=[]):
    if existing:
        dft = existing[-1]
    else:
        dft = default

    # ask to user until valid format
    msg = "Enter the value ({})".format(key)
    value = click.prompt(click.style(msg, fg="red", bg=None), default=dft)
    return value


def ask_for_name(existing=[], fmt=None):
    fmt_no_bracket = re.sub(r'\{*\}*', "", fmt)
    if existing:
        dft = existing[-1]
    else:
        dft = re.sub(r'_([^_]+)$', "_0", fmt_no_bracket)

    # get last id for incrmental
    id_match = re.search(r'(\d+$)', dft)
    if id_match:
        id = int(id_match.group(1)) + 1
        dft = re.sub(r'(\d+$)', str(id), dft)

    # ask to user until valid format
    msg = "Enter the full name ({}) or the ID number".format(fmt)
    while True:
        value = click.prompt(
            click.style(msg, fg="green", bg=None), default=dft)

        # only id was prompted, the should end with an integer
        try:
            id_num = int(value)
            id = "_{}".format(value)
            value = re.sub(r'_([^_]+)$', id, dft)
        except:
            pass

        # name to data
        try:
            value_dict = reverse_format(fmt, value)
            value_dict["filename"] = value
            break
        except:
            warning("Invalid format, the id must end with an integer")
    return value, value_dict


def start_zeamage():
    click.echo(click.style(ascii_app, fg="green", bold=False))
    click.echo(
        click.style(
            "\t" * 7 + "version: " + version + "\n", fg="red", bold=True))


def stop_zeamage():
    message("\n-- END --")
    sys.exit(0)


def not_implemented():
    click.echo(NOT_IMPLEMENTED)
    stop_zeamage()


def get_datetime():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M")


def progress_bar(iterable, length, label):
    return click.progressbar(
        iterable,
        label=click.style("\t\t" + label + "\t", fg="magenta"),
        length=length,
        fill_char="-",
        empty_char=" ",
        color="magenta")
