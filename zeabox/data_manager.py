import logging
import os

import pandas as pd

import cv2

logger = logging.getLogger(__name__)


class DataManager:
    def __init__(self, data, output_path, csv_suffix=""):
        # self.img_patch = data.pop("img_patch", None)
        self.data = pd.DataFrame(
            data)  # convert list of dict to panda dataframe
        self.output_path = output_path
        self.csv_suffix=csv_suffix

    @property
    def data_no_img(self):
        """Drop img column for future export."""
        filter_col = [col for col in self.data.columns if col.startswith("img")]
        return self.data.drop(filter_col, axis=1, errors='ignore')

    def to_csv(self, file="zea_data.csv"):
        """ export all data except the img column"""
        file = "{0}{1}.csv".format(
            self.csv_suffix,
            os.path.basename(os.path.join(self.output_path)))
        file_path = os.path.join(self.output_path, file)
        cols = self.data_no_img.columns.values.tolist()
        df = self.data_no_img[cols]
        sorted_cols = sorted(list(df.columns.values))
        df[sorted_cols].to_csv(
            file_path,
            date_format='%d/%m/%Y',
            float_format='%.2f',
            index=False)

    def _to_img(self, column, dir):
        """util function to export image in a dedicated directory."""
        dir = os.path.join(self.output_path, dir)
        if not os.path.exists(dir):
            os.makedirs(dir)

        for index, row in self.data.iterrows():
            try:
                item_num = "-{}".format(row["item_num"]) if 'item_num' in row.keys() else ""
                file_name = "{0}{1}.png".format(row["filename"],item_num)
                file_path = os.path.join(dir, file_name)
                cv2.imwrite(file_path, row[column])
            except:
                file_name = "{0}.png".format(row["filename"])
                logger.info(
                    "No item found on the image, '{}' will not be exported".
                    format(file_name))

    def to_patch_img(self, dir="img_patch"):
        filter_col = [col for col in self.data.columns if col.startswith(dir)]
        for column in filter_col:
            dir = os.path.join(self.output_path, column)
            if not os.path.exists(dir):
                os.makedirs(dir)

            for index, row in self.data.iterrows():
                try:
                    for i, img in enumerate(row[column]):
                        try:
                            patch_cat = column.split("/")[-1]
                            file_name = "{0}-{1}-{2}.png".format(
                                row["filename"], patch_cat, i)
                            file_path = os.path.join(dir, file_name)
                            cv2.imwrite(file_path, img)
                        except:
                            file_name = "{0}.png".format(row["filename"])
                            logger.info(
                                "No item found on the image, '{}' will not be exported".
                                format(file_name))
                except:
                    logger.info("No image")

    def to_input_img(self, dir="img_input"):
        """export normalized image."""
        self._to_img('img_input', dir)

    def to_normalized_img(self, dir="img_norm"):
        """export normalized image."""
        self._to_img('img', dir)

    def to_summary_img(self, dir="img_summary"):
        """export summary image."""
        self._to_img('img_summary', dir)
