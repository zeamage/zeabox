=======
Credits
=======

Development Lead
----------------

* Jérôme Dury <jerome.dur@flyingsheep.fr>

Contributors
------------

* Jérôme Dury <jerome.dur@flyingsheep.fr>
* Valentin rigal <valentin.rigal@inpt.fr
