#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    'Click>=6.7',
    "opencv-python>=3.4.5.20",
    "pyyaml>=3.13",
    "pandas>=0.23.4",
    "pyusb==1.0.2"
]

setup_requirements = [ ]

test_requirements = [ ]

setup(
    author="Jérôme Dury",
    author_email='jerome.dur@flyingsheep.fr',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    description="Maize phenotyping application tool based on zeamage digital imaging library",
    entry_points={
        'console_scripts': [
            'zeabox=zeabox.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='zeabox',
    name='zeabox',
    packages=find_packages(include=['zeabox']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/farmlab/zeabox',
    version='0.1.0',
    zip_safe=False,
)
