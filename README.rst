==========================
zeabox: Zeamage client application
==========================

Maize phenotyping application tool based on zeamage digital imaging library.

Ear and kernel attributes are important crop varieties traits to know. They are valuable information
and can greatly improve the process of crop selection made by farmers. These attributes are however
very time consuming to measure.


* Free software: MIT license
* Documentation: https://zeamage.frama.io/zeabox

Features
--------

* Data acquisition:
  - high quality image
  - weight
* Data processing:
  - ear weight, size and shape
  - grain weight


Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
