.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Zeamage client application, run this command in your terminal:

.. code-block:: console

    $ pip install zeabox

This is the preferred method to install Zeamage client application, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Zeamage client application can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://github.com/farmlab/zeabox

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://github.com/farmlab/zeabox/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://github.com/farmlab/zeabox
.. _tarball: https://github.com/farmlab/zeabox/tarball/master
